VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "文字数カウンタ"
   ClientHeight    =   4260
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   5130
   LinkTopic       =   "Form1"
   ScaleHeight     =   4260
   ScaleWidth      =   5130
   StartUpPosition =   3  'Windows の既定値
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "ＭＳ ゴシック"
         Size            =   9
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   3  '両方
      TabIndex        =   0
      Top             =   0
      Width           =   4455
   End
   Begin VB.Menu mnuPaste 
      Caption         =   "貼り付け"
   End
   Begin VB.Menu mnuClear 
      Caption         =   "クリア"
   End
   Begin VB.Menu mnuOmake 
      Caption         =   "おまけ"
      Begin VB.Menu mnuReplace 
         Caption         =   "置換"
      End
      Begin VB.Menu mnuCopyAll 
         Caption         =   "全文コピー"
      End
   End
   Begin VB.Menu mnuExit 
      Caption         =   "終了"
   End
   Begin VB.Menu mnuCount 
      Caption         =   "文字数 0"
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Resize()
    If WindowState = vbMinimized Then Exit Sub
    Text1.Width = ScaleWidth
    Text1.Height = ScaleHeight
End Sub

Private Sub mnuClear_Click()
    Text1.Text = ""
End Sub

Private Sub mnuCopyAll_Click()
    Dim strTemp$
    strTemp = Text1.Text
    Clipboard.Clear
    Clipboard.SetText strTemp
End Sub

Private Sub mnuCount_Click()
    If MsgBox("文字数をクリップボードにコピーしますか？", vbYesNo, "確認") = vbNo Then
        Exit Sub
    End If
    Clipboard.Clear
    Clipboard.SetText Format(Len(Text1.Text))
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuPaste_Click()
    Text1.Text = Text1.Text & Clipboard.GetText()
End Sub

Private Sub mnuReplace_Click()
    Dim strFind$
    strFind = InputBox("検索する文字")
    If strFind = "" Then
        Exit Sub
    End If
    Dim strReplace$
    strReplace = InputBox("置換後の文字")
    If MsgBox("以下の条件で置換を実行します" & vbNewLine & vbNewLine _
                & "検索する文字: """ & strFind & """" & vbNewLine & vbNewLine _
                & "置換後の文字: """ & strReplace & """" _
                , vbYesNo Or vbInformation, "確認") = vbNo Then
        Exit Sub
    End If
    Dim strTemp$
    strTemp = Text1.Text
    Text1.Text = Replace(strTemp, strFind, strReplace)
End Sub

Private Sub Text1_Change()
    mnuCount.Caption = "文字数" & Str(Len(Text1.Text))
End Sub
